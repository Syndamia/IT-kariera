using System;
using System.Threading.Tasks;
using AutoMapper;
using FilmWorld.Services.Interfaces;
using FilmWorld.Services.Models.Film;
using FilmWorld.Web.Models.Film;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Authorization;
using FilmWorld.Common;

namespace FilmWorld.Web.Controllers
{
	[Authorize]
	public class FilmController : Controller
	{
		private readonly IMapper _autoMapper;
		private readonly IFilmService _service;

		public FilmController(IMapper autoMapper, IFilmService service)
		{
			this._autoMapper = autoMapper;
			this._service = service;
		}

		[HttpGet]
		public async Task<IActionResult> Index(string query)
		{
			List<FilmServiceModel> serviceModels = (query == null)
				? await this._service.GetAllAsync()
				: await this._service.GetAllByQueryAsync(query);

			List<FilmWebModel> webModel = serviceModels
				.Select(x => this._autoMapper.Map<FilmWebModel>(x))
				.ToList();
			return View(webModel);
		}

		[HttpGet]
		public async Task<IActionResult> Details(string id)
		{
			FilmServiceModel serviceModel = await this._service.GetByIdAsync(Guid.Parse(id));

			FilmWebModel webModel = this._autoMapper.Map<FilmWebModel>(serviceModel);
			return View(webModel);
		}


		[HttpGet]
		[Authorize(Roles = RoleConst.Admin)]
		public IActionResult Create()
		{
			return View();
		}

		[HttpPost]
		[Authorize(Roles = RoleConst.Admin)]
		public async Task<IActionResult> Create(CreateFilmWebModel webModel)
		{
			if (!ModelState.IsValid)
				return View(webModel);

			FilmServiceModel serviceModel = this._autoMapper.Map<FilmServiceModel>(webModel);

			bool result = await this._service.CreateAsync(serviceModel);

			if (result)
				return RedirectToAction("Index");
			else
				return RedirectToAction("Create");
		}

		[HttpGet]
		[Authorize(Roles = RoleConst.Admin)]
		public async Task<IActionResult> Edit(string id)
		{
			FilmServiceModel serviceModel = await this._service.GetByIdAsync(Guid.Parse(id));

			EditFilmWebModel editFilmWebModel = this._autoMapper.Map<EditFilmWebModel>(serviceModel);
			return View(editFilmWebModel);
		}

		[HttpPost]
		[Authorize(Roles = RoleConst.Admin)]
		public async Task<IActionResult> Edit(EditFilmWebModel webModel)
		{
			if (!ModelState.IsValid)
				return View(webModel);

			FilmServiceModel serviceModel = this._autoMapper.Map<FilmServiceModel>(webModel);

			bool result = await this._service.EditAsync(serviceModel);

			if (result)
				return RedirectToAction("Details", new { id = webModel.Id.ToString() });
			else
				return RedirectToAction("Edit", new { id = webModel.Id.ToString() });
		}

		[HttpGet]
		[Authorize(Roles = RoleConst.Admin)]
		public IActionResult Delete()
		{
			return View();
		}

		[HttpPost]
		public async Task<IActionResult> Delete(string id)
		{
			bool result = await this._service.DeleteAsync(Guid.Parse(id));

			if (result)
				return RedirectToAction("Index");
			else
				return RedirectToAction("Delete", new { id = id });
		}
	}
}
