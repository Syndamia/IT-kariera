using FilmWorld.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using FilmWorld.Web.Models.User;
using AutoMapper;
using FilmWorld.Services.Models.User;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using FilmWorld.Common;
using System.Collections.Generic;
using System.Linq;

namespace FilmWorld.Web.Controllers
{
	[Authorize]
	public class AccountController : Controller
	{
		private readonly IMapper _autoMapper;
		private readonly IUserService _userService;

		public AccountController(IMapper autoMapper, IUserService userService)
		{
			this._autoMapper = autoMapper;
			this._userService = userService;
		}

		[HttpGet]
		[Authorize(Roles = RoleConst.Admin)]
		public IActionResult Accounts()
		{
			List<UserWebModel> webModel = this._userService.AllUsers()
				.Select(x => this._autoMapper.Map<UserWebModel>(x))
				.ToList();
			return View(webModel);
		}

		[HttpGet]
		[AllowAnonymous]
		public IActionResult Register()
		{
			return View();
		}

		[HttpPost]
		[AllowAnonymous]
		public async Task<IActionResult> Register(RegisterUserWebModel registerUserWebModel)
		{
			if (!ModelState.IsValid)
				return View(registerUserWebModel);

			RegisterUserServiceModel registerUserServiceModel = this._autoMapper.Map<RegisterUserServiceModel>(registerUserWebModel);

			bool result = await this._userService.RegisterUserAsync(registerUserServiceModel);

			if (result)
				return await this.Login(new LoginUserWebModel { 
						Username = registerUserServiceModel.Username,
						Password = registerUserServiceModel.Password
						});
			else
				return View(registerUserWebModel);
		}

		[HttpGet]
		[AllowAnonymous]
		public IActionResult Login()
		{
			return View();
		}

		[HttpPost]
		[AllowAnonymous]
		public async Task<IActionResult> Login(LoginUserWebModel loginUserWebModel)
		{
			if (!ModelState.IsValid)
				return View(loginUserWebModel);

			LoginUserServiceModel loginUserServiceModel = this._autoMapper.Map<LoginUserServiceModel>(loginUserWebModel);

			bool result = await this._userService.LoginUserAsync(loginUserServiceModel);

			if (result)
				return RedirectToAction("Index", "Home");
			else
				return View(loginUserWebModel);
		}

		[HttpPost]
		public async Task<IActionResult> Logout()
		{
			await this._userService.LogoutAsync();

			return RedirectToAction("Login");
		}

		[HttpGet]
		[AllowAnonymous]
		public async Task<IActionResult> Profile(string username)
		{
			UserServiceModel userServiceModel = await this._userService.GetUserByUsernameAsync(username);

			if (userServiceModel == default(UserServiceModel))
				return RedirectToAction("ErrorNotFound", "Home");

			UserWebModel userWebModel = this._autoMapper.Map<UserWebModel>(userServiceModel);

			return View(userWebModel);
		}

		[HttpGet]
		public async Task<IActionResult> Edit(string username)
		{
			UserServiceModel userServiceModel;
			if (username == null)
			{
				userServiceModel = await this._userService.GetUserByClaimsAsync(this.HttpContext.User);
			}
			else
			{
				userServiceModel = await this._userService.GetUserByUsernameAsync(username);
			}

			if (userServiceModel == default(UserServiceModel))
				return RedirectToAction("ErrorNotFound", "Home");

			EditUserWebModel editUserWebModel = this._autoMapper.Map<EditUserWebModel>(userServiceModel);
			return View(editUserWebModel);
		}

		[HttpPost]
		public async Task<IActionResult> Edit(string username, EditUserWebModel editUserWebModel)
		{
			if (!ModelState.IsValid)
				return View(editUserWebModel);

			if (!this._userService.IsSignedIn(HttpContext.User))
					return RedirectToAction("Login");

			UserServiceModel loggedInUser = await this._userService.GetUserByClaimsAsync(HttpContext.User);
			UserServiceModel userServiceModel = this._autoMapper.Map<UserServiceModel>(editUserWebModel);

			bool result = false;
			if (username == null)
			{
				result = await this._userService.EditUserAsync(HttpContext.User, userServiceModel);
			}
			else
			{
				result = await this._userService.EditUserAsync(username, userServiceModel);
			}

			if (result)
			{
				if (loggedInUser.Username != editUserWebModel.Username)
					await this._userService.LogoutAsync();

				return RedirectToAction("Profile", new { username = editUserWebModel.Username });
			}
			else
				return RedirectToAction("Profile", new { username = loggedInUser.Username });
		}

		[HttpPost]
		public async Task<IActionResult> Delete()
		{
			await this._userService.LogoutAsync();
			bool result = await this._userService.DeleteUserAsync(HttpContext.User);

			if (result)
				return RedirectToAction("Login");
			else
				return RedirectToAction("Index", "Home");
		}
	}
}
