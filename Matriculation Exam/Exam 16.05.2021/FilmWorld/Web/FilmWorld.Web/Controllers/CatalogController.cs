using System;
using System.Threading.Tasks;
using AutoMapper;
using FilmWorld.Services.Interfaces;
using FilmWorld.Services.Models.Catalog;
using FilmWorld.Web.Models.Catalog;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Authorization;
using System.Security.Claims;

namespace FilmWorld.Web.Controllers
{
    [Authorize]
	public class CatalogController : Controller
	{
		private readonly IMapper _autoMapper;
		private readonly ICatalogService _service;
		private readonly IUserService _userService;
		private readonly IFilmService _filmService;

		public CatalogController(IMapper autoMapper, ICatalogService service, IUserService userService, IFilmService filmService)
		{
			this._autoMapper = autoMapper;
			this._service = service;
			this._userService = userService;
			this._filmService = filmService;
		}

		[HttpGet]
		public IActionResult Index()
		{
			string userId = HttpContext.User.FindFirst(ClaimTypes.NameIdentifier)?.Value;
			HashSet<CatalogServiceModel> serviceModels = this._service.GetUserCatalogs(Guid.Parse(userId));

			List<CatalogWebModel> webModel = serviceModels
				.Select(x => this._autoMapper.Map<CatalogWebModel>(x))
				.ToList();
			return View(webModel);
		}

		[HttpGet]
		public async Task<IActionResult> Details(string id)
		{
			CatalogServiceModel serviceModel = await this._service.GetByIdAsync(Guid.Parse(id));
			serviceModel.Films = await this._service.GetAllFilms(Guid.Parse(id));

			CatalogWebModel webModel = this._autoMapper.Map<CatalogWebModel>(serviceModel);
			return View(webModel);
		}


		[HttpGet]
		public IActionResult Create()
		{
			return View();
		}

		[HttpPost]
		public async Task<IActionResult> Create(CreateCatalogWebModel webModel)
		{
			if (!ModelState.IsValid)
				return View(webModel);

			CatalogServiceModel serviceModel = this._autoMapper.Map<CatalogServiceModel>(webModel);
			
			string userId = HttpContext.User.FindFirst(ClaimTypes.NameIdentifier)?.Value;

			bool result = await this._service.CreateAsync(serviceModel, Guid.Parse(userId));

			if (result)
				return RedirectToAction("Index");
			else
				return RedirectToAction("Create");
		}

		[HttpGet]
		public async Task<IActionResult> Edit(string id)
		{
			CatalogServiceModel serviceModel = await this._service.GetByIdAsync(Guid.Parse(id));

			EditCatalogWebModel editCatalogWebModel = this._autoMapper.Map<EditCatalogWebModel>(serviceModel);
			return View(editCatalogWebModel);
		}

		[HttpPost]
		public async Task<IActionResult> Edit(EditCatalogWebModel webModel)
		{
			if (!ModelState.IsValid)
				return View(webModel);

			CatalogServiceModel serviceModel = this._autoMapper.Map<CatalogServiceModel>(webModel);

			bool result = await this._service.EditAsync(serviceModel);

			if (result)
				return RedirectToAction("Details", new { id = webModel.Id.ToString() });
			else
				return RedirectToAction("Edit", new { id = webModel.Id.ToString() });
		}

		[HttpGet]
		public IActionResult AddFilm(string id)
		{
			string userId = HttpContext.User.FindFirst(ClaimTypes.NameIdentifier)?.Value;

			List<CatalogWebModel> webModel = this._service.GetUserCatalogs(Guid.Parse(userId))
				.Select(c => this._autoMapper.Map<CatalogWebModel>(c))
				.ToList();
			return View(webModel);
		}

		[HttpPost]
		public async Task<IActionResult> AddFilm(string filmId, string[] catalogIds)
		{
			bool result = await this._service.AddFilmToCatalogs(Guid.Parse(filmId), catalogIds.Select(c => Guid.Parse(c)).ToArray());

			if (result)
				return RedirectToAction("Index");
			else
				return RedirectToAction("AddFilm", new { id = filmId });
		}

		[HttpGet]
		public IActionResult Delete()
		{
			return View();
		}

		[HttpPost]
		public async Task<IActionResult> Delete(string id)
		{
			bool result = await this._service.DeleteAsync(Guid.Parse(id));

			if (result)
				return RedirectToAction("Index");
			else
				return RedirectToAction("Delete", new { id = id });
		}
	}
}
