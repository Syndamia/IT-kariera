using AutoMapper;
using FilmWorld.Services.Models.Catalog;
using FilmWorld.Web.Models.Catalog;

namespace FilmWorld.Web.Configurations
{
	public class ControllerCatalogMappings : Profile
	{
		public ControllerCatalogMappings()
		{
			CreateMap<CatalogServiceModel, CatalogWebModel>();
			CreateMap<CreateCatalogWebModel, CatalogServiceModel>();
			CreateMap<CatalogServiceModel, EditCatalogWebModel>();
			CreateMap<EditCatalogWebModel, CatalogServiceModel>();
		}
	}
}
