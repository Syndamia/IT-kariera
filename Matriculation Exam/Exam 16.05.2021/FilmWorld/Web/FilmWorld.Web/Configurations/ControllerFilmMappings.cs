using AutoMapper;
using FilmWorld.Services.Models.Film;
using FilmWorld.Web.Models.Film;

namespace FilmWorld.Web.Configurations
{
	public class ControllerFilmMappings : Profile
	{
		public ControllerFilmMappings()
		{
			CreateMap<FilmServiceModel, FilmWebModel>();
			CreateMap<CreateFilmWebModel, FilmServiceModel>();
			CreateMap<FilmServiceModel, EditFilmWebModel>();
			CreateMap<EditFilmWebModel, FilmServiceModel>();
		}
	}
}
