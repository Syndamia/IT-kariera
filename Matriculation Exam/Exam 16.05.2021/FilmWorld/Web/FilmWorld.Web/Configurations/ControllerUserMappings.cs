using AutoMapper;
using FilmWorld.Services.Models.User;
using FilmWorld.Web.Models.User;

namespace FilmWorld.Services.Configurations
{
	public class ControllerUserMappings : Profile
	{
		public ControllerUserMappings()
		{
			CreateMap<RegisterUserWebModel, RegisterUserServiceModel>();
			CreateMap<LoginUserWebModel, LoginUserServiceModel>();
			CreateMap<UserServiceModel, UserWebModel>();
			CreateMap<UserServiceModel, EditUserWebModel>();
			CreateMap<EditUserWebModel, UserServiceModel>();
		}
	}
}
