using System;

namespace FilmWorld.Web.Models.User
{
    public class UserWebModel
	{
		public Guid Id { get; set; }
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public string Username { get; set; }
	}
}
