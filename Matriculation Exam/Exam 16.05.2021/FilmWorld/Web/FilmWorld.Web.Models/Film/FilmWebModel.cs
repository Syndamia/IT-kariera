using System;
using System.Collections.Generic;
using FilmWorld.Web.Models.Catalog;

namespace FilmWorld.Web.Models.Film
{
    public class FilmWebModel : BaseWebModel
	{
		public string Name { get; set; }
		public string PhotoURL { get; set; }
		public string Description { get; set; }
		public DateTime PremiereDate { get; set; }
		public HashSet<CatalogWebModel> Catalogs { get; set; }
	}
}
