using System;
using Microsoft.AspNetCore.Http;
using System.ComponentModel.DataAnnotations;

namespace FilmWorld.Web.Models.Film
{
    public class CreateFilmWebModel
	{
		[Required]
		[MinLength(1)]
		[MaxLength(64)]
		public string Name { get; set; }

		[Required]
		public IFormFile Photo { get; set; }

		[Required]
		[MinLength(1)]
		[MaxLength(255)]
		public string Description { get; set; }

		[Required]
		public DateTime PremiereDate { get; set; }
	}
}
