using System.Collections.Generic;
using FilmWorld.Web.Models.Film;

namespace FilmWorld.Web.Models.Catalog
{
    public class CatalogWebModel : BaseWebModel
	{
		public string Name { get; set; }
		public string Description { get; set; }
		public HashSet<FilmWebModel> Films { get; set; }
	}
}
