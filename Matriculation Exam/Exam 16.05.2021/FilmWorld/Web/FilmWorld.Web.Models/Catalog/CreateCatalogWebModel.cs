using System.ComponentModel.DataAnnotations;

namespace FilmWorld.Web.Models.Catalog
{
    public class CreateCatalogWebModel
	{
		[Required]
		[MinLength(1)]
		[MaxLength(64)]
		public string Name { get; set; }

		[Required]
		[MinLength(1)]
		[MaxLength(255)]
		public string Description { get; set; }
	}
}
