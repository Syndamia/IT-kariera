using System;
using System.ComponentModel.DataAnnotations;

namespace FilmWorld.Web.Models.Catalog
{
    public class EditCatalogWebModel : BaseWebModel
	{
		[Required]
		[MinLength(1)]
		[MaxLength(255)]
		public string Name { get; set; }

		[Required]
		[MinLength(1)]
		[MaxLength(255)]
		public string Description { get; set; }

		public Guid AuthorId { get; set; }
	}
}
