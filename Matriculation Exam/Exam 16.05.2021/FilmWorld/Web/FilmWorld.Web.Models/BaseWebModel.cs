using System;

namespace FilmWorld.Web.Models
{
	public abstract class BaseWebModel
	{
		public Guid Id { get; set; }
	}
}
