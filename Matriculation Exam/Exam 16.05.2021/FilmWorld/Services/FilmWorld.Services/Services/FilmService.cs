using FilmWorld.Services.Interfaces;
using AutoMapper;
using FilmWorld.Data;
using FilmWorld.Data.Models;
using FilmWorld.Services.Models.Film;
using System.Threading.Tasks;
using System;
using FilmWorld.Common;
using System.Collections.Generic;
using Microsoft.AspNetCore.Http;
using System.Linq;

namespace FilmWorld.Services.Services
{
    public class FilmService : BaseService<Film, FilmServiceModel>, IFilmService
	{
		private readonly ICloudinaryService _cloud;

		public FilmService(IMapper autoMapper, FilmWorldContext context, ICloudinaryService cloud)
			:base(autoMapper, context)
		{
			this._cloud = cloud;
		}

		public async Task<List<FilmServiceModel>> GetAllByQueryAsync(string query)
		{
			return (await this.GetAllAsync())
				.Where(x => x.Name.Contains(query))
				.ToList();
		}

		public override async Task<bool> CreateAsync(FilmServiceModel serviceModel)
		{
			if (serviceModel == null)
				throw new ArgumentNullException(ErrorMessages.NullObject(typeof(FilmServiceModel)));

			Film newEntity = this._autoMapper.Map<Film>(serviceModel);
			newEntity.PhotoURL = (await this._cloud.UploadFilesToCloud(new List<IFormFile>() { serviceModel.Photo }))[0];

			await this.GetDbSet()
				.AddAsync(newEntity);

			return await this.SaveChangesAsync();
		}

		public override async Task<bool> EditAsync(FilmServiceModel serviceModel)
		{
			if (serviceModel == null)
				throw new ArgumentNullException(ErrorMessages.NullObject(typeof(FilmServiceModel)));

			Film entity = this._autoMapper.Map<Film>(serviceModel);
			entity.PhotoURL = (await this._cloud.UploadFilesToCloud(new List<IFormFile>() { serviceModel.Photo }))[0];

			this._context.Update(entity);

			return await this.SaveChangesAsync();
		}
	}
}
