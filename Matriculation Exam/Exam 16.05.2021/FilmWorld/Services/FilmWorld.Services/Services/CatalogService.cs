using FilmWorld.Services.Interfaces;
using AutoMapper;
using FilmWorld.Data;
using FilmWorld.Data.Models;
using FilmWorld.Services.Models.Catalog;
using System.Collections.Generic;
using System;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using FilmWorld.Common;
using FilmWorld.Services.Models.Film;

namespace FilmWorld.Services.Services
{
    public class CatalogService : BaseService<Catalog, CatalogServiceModel>, ICatalogService
	{
		public CatalogService(IMapper autoMapper, FilmWorldContext context)
			:base(autoMapper, context)
		{ }

		public override async Task<CatalogServiceModel> GetByIdAsync(Guid id)
		{
			if (id == Guid.Empty)
				throw new ArgumentNullException(ErrorMessages.NullObject(typeof(Guid)));

			Catalog entity = await this.GetDbSetFull()
				.FirstOrDefaultAsync(x => x.Id == id);

			return this._autoMapper.Map<CatalogServiceModel>(entity);
		}

		// This is sheer stupidity, but I was getting an Automapper StackOverflow exception
		// when I add the Include in GetDbSetFull and I wasted too much time on this
		public async Task<HashSet<FilmServiceModel>> GetAllFilms(Guid catalogId)
		{
			var items = this._context.Set<CatalogsFilms>()
				.Where(x => x.CatalogId == catalogId)
				.ToHashSet();
			HashSet<FilmServiceModel> result = new HashSet<FilmServiceModel>();
			foreach (var item in items)
			{
				var film = await this._context.Films.FindAsync(item.FilmId);
				result.Add(this._autoMapper.Map<FilmServiceModel>(film));
			}
			return result;
		}

		public HashSet<CatalogServiceModel> GetUserCatalogs(Guid userId)
		{
			return this.GetDbSetFull()
				.Where(c => c.AuthorId == userId)
				.Select(c => this._autoMapper.Map<CatalogServiceModel>(c))
				.ToHashSet();
		}

		public async Task<bool> CreateAsync(CatalogServiceModel serviceModel, Guid authorId)
		{
			Catalog catalog = this._autoMapper.Map<Catalog>(serviceModel);
			catalog.Author = this._context.Users.Find(authorId);

			await this._context.Catalogs
				.AddAsync(catalog);

			return await this.SaveChangesAsync();
		}

		public override async Task<bool> EditAsync(CatalogServiceModel serviceModel)
		{
			if (serviceModel == null)
				throw new ArgumentNullException(ErrorMessages.NullObject(typeof(CatalogServiceModel)));

			Catalog entity = this._autoMapper.Map<Catalog>(serviceModel);
			entity.Author = await this._context.Users
				.FirstOrDefaultAsync(x => x.Id == serviceModel.AuthorId);
			entity.AuthorId = serviceModel.AuthorId;

			this._context.Update(entity);

			return await this.SaveChangesAsync();
		}

		public async Task<bool> AddFilmToCatalogs(Guid filmId, Guid[] catalogIds)
		{
			Film toAdd = await this._context.Films
				.FindAsync(filmId);

			foreach (Guid id in catalogIds)
			{
				Catalog catalog = await this.GetDbSetFull()
					.FirstOrDefaultAsync(c => c.Id == id);

				// This is sheer stupidity, but I was getting an Automapper StackOverflow exception
				// when I add the Include in GetDbSetFull and I wasted too much time on this
				if (!this._context.Set<CatalogsFilms>().Any(x => x.FilmId == filmId && x.CatalogId == id))
				{
					CatalogsFilms cf = new CatalogsFilms() {
						FilmId = filmId,
						Film = toAdd,
						CatalogId = id,
						Catalog = catalog
					};
					this._context.Set<CatalogsFilms>().Add(cf);

					if (!await this.SaveChangesAsync())
						return false;
				}
			}

			return true;
		}

		private IQueryable<Catalog> GetDbSetFull()
		{
			return this.GetDbSet()
				.Include(x => x.Author)
				.Include(x => x.Films);
		}
	}
}
