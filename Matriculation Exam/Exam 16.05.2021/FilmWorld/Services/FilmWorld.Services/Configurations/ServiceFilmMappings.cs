using AutoMapper;
using FilmWorld.Data.Models;
using FilmWorld.Services.Models.Film;

namespace FilmWorld.Services.Configurations
{
	public class ServiceFilmMappings : Profile
	{
		public ServiceFilmMappings()
		{
			CreateMap<FilmServiceModel, Film>();
			CreateMap<Film, FilmServiceModel>()
				.ForMember(dest => dest.Catalogs, src => src.Ignore());
		}
	}
}
