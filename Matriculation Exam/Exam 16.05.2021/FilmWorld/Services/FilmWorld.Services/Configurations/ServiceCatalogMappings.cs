using System.Linq;
using AutoMapper;
using FilmWorld.Data.Models;
using FilmWorld.Services.Models.Catalog;

namespace FilmWorld.Services.Configurations
{
	public class ServiceCatalogMappings : Profile
	{
		public ServiceCatalogMappings()
		{
			CreateMap<CatalogServiceModel, Catalog>();
			CreateMap<Catalog, CatalogServiceModel>()
				.ForMember(dest => dest.Films, src => src.MapFrom(p => p.Films.Select(x => x.Film).ToHashSet()));
		}
	}
}
