using AutoMapper;
using FilmWorld.Data.Models;
using FilmWorld.Services.Models.User;

namespace FilmWorld.Services.Configurations
{
	public class ServiceUserMappings : Profile
	{
		public ServiceUserMappings()
		{
			CreateMap<RegisterUserServiceModel, User>();
			CreateMap<User, UserServiceModel>();
			CreateMap<UserServiceModel, User>();
		}
	}
}
