using System.Collections.Generic;
using System.Threading.Tasks;
using FilmWorld.Data.Models;
using FilmWorld.Services.Models.Film;

namespace FilmWorld.Services.Interfaces
{
	public interface IFilmService : IBaseService<Film, FilmServiceModel>
	{
		Task<List<FilmServiceModel>> GetAllByQueryAsync(string query);
	}
}
