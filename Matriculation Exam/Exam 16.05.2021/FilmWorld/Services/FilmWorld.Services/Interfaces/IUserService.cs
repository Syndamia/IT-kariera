using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using FilmWorld.Services.Models.User;

namespace FilmWorld.Services.Interfaces
{
	public interface IUserService
	{
		List<UserServiceModel> AllUsers();

		Task<bool> RegisterUserAsync(RegisterUserServiceModel registerUserServiceModel);

		Task<bool> LoginUserAsync(LoginUserServiceModel loginUserServiceModel);

		Task LogoutAsync();

		Task<UserServiceModel> GetUserByUsernameAsync(string username);

		Task<UserServiceModel> GetUserByClaimsAsync(ClaimsPrincipal claimsPrincipal);

		Task<bool> EditUserAsync(ClaimsPrincipal claimsPrincipal, UserServiceModel userServiceModel);

		Task<bool> EditUserAsync(string username, UserServiceModel userServiceModel);

		Task<bool> DeleteUserAsync(ClaimsPrincipal claimsPrincipal);

		bool IsSignedIn(ClaimsPrincipal claimsPrincipal);
	}
}
