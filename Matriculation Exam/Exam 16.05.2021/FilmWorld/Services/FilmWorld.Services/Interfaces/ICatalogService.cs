using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using FilmWorld.Data.Models;
using FilmWorld.Services.Models.Catalog;
using FilmWorld.Services.Models.Film;

namespace FilmWorld.Services.Interfaces
{
	public interface ICatalogService : IBaseService<Catalog, CatalogServiceModel>
	{
		HashSet<CatalogServiceModel> GetUserCatalogs(Guid userId);
		Task<bool> CreateAsync(CatalogServiceModel serviceModel, Guid authorId);
		Task<bool> AddFilmToCatalogs(Guid filmId, Guid[] catalogIds);
		Task<HashSet<FilmServiceModel>> GetAllFilms(Guid catalogId);
	}
}
