using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace FilmWorld.Services.Interfaces
{
	public interface ICloudinaryService
	{
		Task<List<string>> UploadFilesToCloud(List<IFormFile> formFiles);
	}
}
