using System;

namespace FilmWorld.Services.Models
{
	public abstract class BaseServiceModel
	{
		public Guid Id { get; set; }
	}
}
