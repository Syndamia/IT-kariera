using System;
using System.Collections.Generic;
using FilmWorld.Services.Models.Film;

namespace FilmWorld.Services.Models.Catalog
{
    public class CatalogServiceModel : BaseServiceModel
	{
		public string Name { get; set; }
		public string Description { get; set; }
		public HashSet<FilmServiceModel> Films { get; set; }
		public Guid AuthorId { get; set; }
	}
}
