using System;
using System.Collections.Generic;
using FilmWorld.Services.Models.Catalog;
using Microsoft.AspNetCore.Http;

namespace FilmWorld.Services.Models.Film
{
    public class FilmServiceModel : BaseServiceModel
	{
		public string Name { get; set; }
		public string PhotoURL { get; set; }
		public IFormFile Photo { get; set; }
		public string Description { get; set; }
		public DateTime PremiereDate { get; set; }
		public HashSet<CatalogServiceModel> Catalogs { get; set; }
	}
}
