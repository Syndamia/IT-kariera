using System;

namespace FilmWorld.Services.Models.User
{
    public class UserServiceModel
	{
		public Guid Id { get; set; }
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public string Username { get; set; }
	}
}
