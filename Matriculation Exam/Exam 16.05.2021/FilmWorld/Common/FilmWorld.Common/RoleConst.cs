using System.Collections.Generic;

namespace FilmWorld.Common
{
	public static class RoleConst
	{
		public const string User = "User";
		public const string Admin = "Administrator";

		public static List<string> GetAllNames()
		{
			return new List<string>() {
				User, Admin
			};
		}
	}
}
