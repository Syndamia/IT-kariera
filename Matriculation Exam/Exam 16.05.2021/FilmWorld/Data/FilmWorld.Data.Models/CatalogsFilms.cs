using System;

namespace FilmWorld.Data.Models
{
    public class CatalogsFilms
	{
		public Guid CatalogId { get; set; }
		public Catalog Catalog { get; set; }

		public Guid FilmId { get; set; }
		public Film Film { get; set; }
	}
}
