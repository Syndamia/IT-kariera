using System;
using System.Collections.Generic;

namespace FilmWorld.Data.Models
{
	public class Film : BaseModel
	{
		public string Name { get; set; }
		public string PhotoURL { get; set; }
		public string Description { get; set; }
		public DateTime PremiereDate { get; set; }
		public HashSet<CatalogsFilms> Catalogs { get; set; } = new HashSet<CatalogsFilms>();
	}
}
