using System;
using System.Collections.Generic;

namespace FilmWorld.Data.Models
{
    public class Catalog : BaseModel
	{
		public string Name { get; set; }
		public string Description { get; set; }
		public HashSet<CatalogsFilms> Films { get; set; } = new HashSet<CatalogsFilms>();

		public Guid AuthorId { get; set; }
		public User Author { get; set; }
	}
}
