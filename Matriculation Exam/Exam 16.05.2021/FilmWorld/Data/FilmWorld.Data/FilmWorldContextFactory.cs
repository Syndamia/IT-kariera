using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using System.IO;

namespace FilmWorld.Data
{
	public class FilmWorldContextFactory : IDesignTimeDbContextFactory<FilmWorldContext>
	{
		public FilmWorldContext CreateDbContext(string[] args)
		{
			var configuration = new ConfigurationBuilder()
				.SetBasePath(Directory.GetCurrentDirectory() + "/../../Web/FilmWorld.Web/")
				.AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
				.AddJsonFile("appsettings.Development.json", optional: true)
				.Build();

			var optionsBuilder = new DbContextOptionsBuilder<FilmWorldContext>()
				.UseNpgsql(configuration.GetConnectionString("LocalDBConnection"));

			return new FilmWorldContext(optionsBuilder.Options);
		}
	}
}
