﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Data.Migrations
{
    public partial class UserCatalogs : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<Guid>(
                name: "UserId",
                table: "Catalogs",
                type: "uuid",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Catalogs_UserId",
                table: "Catalogs",
                column: "UserId");

            migrationBuilder.AddForeignKey(
                name: "FK_Catalogs_AspNetUsers_UserId",
                table: "Catalogs",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Catalogs_AspNetUsers_UserId",
                table: "Catalogs");

            migrationBuilder.DropIndex(
                name: "IX_Catalogs_UserId",
                table: "Catalogs");

            migrationBuilder.DropColumn(
                name: "UserId",
                table: "Catalogs");
        }
    }
}
