﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Data.Migrations
{
    public partial class UserCatalogsNew : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Catalogs_AspNetUsers_UserId",
                table: "Catalogs");

            migrationBuilder.RenameColumn(
                name: "UserId",
                table: "Catalogs",
                newName: "AuthorId");

            migrationBuilder.RenameIndex(
                name: "IX_Catalogs_UserId",
                table: "Catalogs",
                newName: "IX_Catalogs_AuthorId");

            migrationBuilder.AddForeignKey(
                name: "FK_Catalogs_AspNetUsers_AuthorId",
                table: "Catalogs",
                column: "AuthorId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Catalogs_AspNetUsers_AuthorId",
                table: "Catalogs");

            migrationBuilder.RenameColumn(
                name: "AuthorId",
                table: "Catalogs",
                newName: "UserId");

            migrationBuilder.RenameIndex(
                name: "IX_Catalogs_AuthorId",
                table: "Catalogs",
                newName: "IX_Catalogs_UserId");

            migrationBuilder.AddForeignKey(
                name: "FK_Catalogs_AspNetUsers_UserId",
                table: "Catalogs",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
