﻿using System;
using FilmWorld.Data.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace FilmWorld.Data
{
	public class FilmWorldContext : IdentityDbContext<User, IdentityRole<Guid>, Guid>
	{
		public FilmWorldContext(DbContextOptions<FilmWorldContext> options)
			: base(options) { }

		public DbSet<Film> Films { get; set; }
		public DbSet<Catalog> Catalogs { get; set; }

		protected override void OnModelCreating(ModelBuilder builder)
		{
			builder.Entity<User>()
				.HasIndex(x => x.UserName)
				.IsUnique();

			builder.Entity<CatalogsFilms>().HasKey(x => new { x.CatalogId, x.FilmId});

			builder.Entity<CatalogsFilms>()
				.HasOne(c => c.Catalog)
				.WithMany(cf => cf.Films)
				.HasForeignKey(c => c.CatalogId);

			builder.Entity<CatalogsFilms>()
				.HasOne(c => c.Film)
				.WithMany(cf => cf.Catalogs)
				.HasForeignKey(c => c.FilmId);

			base.OnModelCreating(builder);
		}
	}
}
