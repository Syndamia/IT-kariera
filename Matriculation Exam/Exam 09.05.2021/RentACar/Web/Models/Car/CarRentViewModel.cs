using System;

namespace RentACar.Web.Models.Car
{
	public class CarRentViewModel
	{
		public Guid Id { get; set; }
		public string Brand { get; set; }
		public string Model { get; set; }
		public int Year { get; set; }
		public double RentPricePerDay { get; set; }
		public double TotalPrice { get; set; }
		public string Photo { get; set; }
	}
}
