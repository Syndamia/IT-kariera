namespace RentACar.Web.Models.User
{
	public class UserViewModel
	{
		public string FirstName { get; set; }
		public string MiddleName { get; set; }
		public string LastName { get; set; }
		public string EGN { get; set; }
		public string PhoneNumber { get; set; }
		public string Email { get; set; }
		public string Username { get; set; }
	}
}
