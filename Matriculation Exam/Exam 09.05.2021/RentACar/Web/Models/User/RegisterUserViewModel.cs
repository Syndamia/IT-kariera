using System.ComponentModel.DataAnnotations;

namespace RentACar.Web.Models.User
{
	public class RegisterUserViewModel
	{
		[Required]
		[MinLength(1)]
		public string FirstName { get; set; }

		[Required]
		[MinLength(1)]
		public string MiddleName { get; set; }

		[Required]
		[MinLength(1)]
		public string LastName { get; set; }

		[Required]
		[MinLength(1)]
		public string EGN { get; set; }

		[Required]
		[Phone]
		[MinLength(1)]
		public string PhoneNumber { get; set; }

		[Required]
		[EmailAddress]
		[MinLength(1)]
		public string Email { get; set; }

		[Required]
		[MinLength(1)]
		public string Username { get; set; }

		[Required]
		[MinLength(3)]
		public string Password { get; set; }
	}
}
