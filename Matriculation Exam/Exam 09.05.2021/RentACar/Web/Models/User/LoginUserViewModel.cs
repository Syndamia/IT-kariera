using System.ComponentModel.DataAnnotations;

namespace RentACar.Web.Models.User
{
	public class LoginUserViewModel
	{
		[Required]
		[MinLength(1)]
		public string Username { get; set; }

		[Required]
		[MinLength(1)]
		public string Password { get; set; }
	}
}
