using System;
using System.Linq;
using RentACar.Common;
using RentACar.Data;
using RentACar.Data.Models;
using RentACar.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace RentACar.Web
{
	public class Startup
	{
		public Startup(IConfiguration configuration)
		{
			Configuration = configuration;
		}

		public IConfiguration Configuration { get; }

		// This method gets called by the runtime. Use this method to add services to the container.
		public void ConfigureServices(IServiceCollection services)
		{
			services.AddControllersWithViews();
			services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());

			/*
			 * Dependency Injection configuration
			 */

			services.AddTransient<UserService>();
			services.AddTransient<CarService>();
			services.AddTransient<CarRentService>();
			services.AddTransient<CloudinaryService>(options =>
				new CloudinaryService(
					cloudName: this.Configuration.GetSection("Cloud").GetSection("cloudName").Value,
					apiKey: this.Configuration.GetSection("Cloud").GetSection("apiKey").Value,
					apiSecret: this.Configuration.GetSection("Cloud").GetSection("apiSecret").Value
					)
				);

			/* 
			 * Database configuration
			 */

			services.AddDbContext<RentACarContext>(options =>
				options.UseNpgsql(this.Configuration.GetConnectionString("LocalDBConnection")));

			// Needed for SignInManager and UserManager
			services.AddIdentity<User, IdentityRole<Guid>>(options =>
			{
				options.SignIn.RequireConfirmedAccount = false;

				// Password settings
				options.Password.RequireDigit = false;
				options.Password.RequireLowercase = false;
				options.Password.RequireNonAlphanumeric = false;
				options.Password.RequireUppercase = false;
				options.Password.RequiredLength = 3;
				options.Password.RequiredUniqueChars = 0;
			}).AddRoles<IdentityRole<Guid>>()
				.AddEntityFrameworkStores<RentACarContext>();
		}

		// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
		public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
		{
			if (env.IsDevelopment())
			{
				app.UseDeveloperExceptionPage();
			}
			else
			{
				app.UseExceptionHandler("/Home/Error");
				// The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
				app.UseHsts();
			}

			app.UseStaticFiles();

			app.UseRouting();

			app.UseAuthentication();
			app.UseAuthorization();

			app.UseEndpoints(endpoints =>
			{
				endpoints.MapControllerRoute(
					name: "default",
					pattern: "{controller=Home}/{action=Index}/{id?}");
			});

			/* 
			 * Make sure that the database is migrated
			 * and that the User and Admin role exist in database 
			 */

			using var serviceScope = app.ApplicationServices.CreateScope();
			using var dbContext = serviceScope.ServiceProvider.GetRequiredService<RentACarContext>();

			dbContext.Database.Migrate();

			var roleManager = (RoleManager<IdentityRole<Guid>>)serviceScope.ServiceProvider.GetService(typeof(RoleManager<IdentityRole<Guid>>));
			foreach (string name in RoleConst.GetAll())
			{
				if (!dbContext.Roles.Any(x => x.Name == name))
				{
					IdentityRole<Guid> role = new IdentityRole<Guid>() { Name = name };
					roleManager.CreateAsync(role).Wait();
				}
			}

			foreach (string name in RentStatusConst.GetAll())
			{
				if (!dbContext.RentStatuses.Any(x => x.Name == name))
				{
					RentStatus rentStatus = new RentStatus() { Id = Guid.NewGuid(), Name = name };
					dbContext.RentStatuses.Add(rentStatus);
					dbContext.SaveChanges();
				}
			}
		}
	}
}
