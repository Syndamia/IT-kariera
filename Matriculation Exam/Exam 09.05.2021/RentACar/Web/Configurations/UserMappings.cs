using AutoMapper;
using RentACar.Services.Models.User;
using RentACar.Web.Models.User;

namespace RentACar.Services.Configurations
{
	public class UserMappings : Profile
	{
		public UserMappings()
		{
			CreateMap<RegisterUserViewModel, RegisterUserServiceModel>();
			CreateMap<LoginUserViewModel, LoginUserServiceModel>();
			CreateMap<UserServiceModel, UserViewModel>();
			CreateMap<UserServiceModel, EditUserViewModel>();
			CreateMap<EditUserViewModel, EditUserServiceModel>();
		}
	}
}
