using AutoMapper;
using RentACar.Services.Models.Car;
using RentACar.Web.Models.Car;

namespace RentACar.Services.Configurations
{
	public class CarMappings : Profile
	{
		public CarMappings()
		{
			CreateMap<CarServiceModel, CarViewModel>();
			CreateMap<CarServiceModel, CarRentViewModel>();
		}
	}
}
