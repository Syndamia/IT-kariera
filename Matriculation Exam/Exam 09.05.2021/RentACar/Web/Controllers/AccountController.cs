using RentACar.Services;
using Microsoft.AspNetCore.Mvc;
using RentACar.Web.Models.User;
using AutoMapper;
using RentACar.Services.Models.User;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;

namespace RentACar.Web.Controllers
{
	[Authorize]
	public class AccountController : Controller
	{
		private readonly IMapper _autoMapper;
		private readonly UserService _userService;

		public AccountController(IMapper autoMapper, UserService userService)
		{
			this._autoMapper = autoMapper;
			this._userService = userService;
		}

		[HttpGet]
		[AllowAnonymous]
		public IActionResult Register()
		{
			return View();
		}

		[HttpPost]
		[AllowAnonymous]
		public async Task<IActionResult> Register(RegisterUserViewModel registerUserViewModel)
		{
			if (!ModelState.IsValid)
				return View(registerUserViewModel);

			RegisterUserServiceModel registerUserServiceModel = this._autoMapper.Map<RegisterUserServiceModel>(registerUserViewModel);

			bool result = await this._userService.RegisterUserAsync(registerUserServiceModel);

			if (result)
				return await Login(new LoginUserViewModel { 
						Username = registerUserServiceModel.Username,
						Password = registerUserServiceModel.Password
						});
			else
				return View(registerUserViewModel);
		}

		[HttpGet]
		[AllowAnonymous]
		public IActionResult Login()
		{
			return View();
		}

		[HttpPost]
		[AllowAnonymous]
		public async Task<IActionResult> Login(LoginUserViewModel loginUserViewModel)
		{
			if (!ModelState.IsValid)
				return View(loginUserViewModel);

			LoginUserServiceModel loginUserServiceModel = this._autoMapper.Map<LoginUserServiceModel>(loginUserViewModel);

			bool result = await this._userService.LoginUserAsync(loginUserServiceModel);

			if (result)
				return RedirectToAction("Index", "Home");
			else
				return View(loginUserViewModel);
		}

		[HttpPost]
		public async Task<IActionResult> Logout()
		{
			await this._userService.LogoutAsync();

			return RedirectToAction("Login");
		}

		[HttpGet]
		public async Task<IActionResult> Profile(string username)
		{
			UserServiceModel userServiceModel = await this._userService.GetUserByUsernameAsync(username);

			if (userServiceModel == default(UserServiceModel))
				return RedirectToAction("Login");

			UserViewModel userViewModel = this._autoMapper.Map<UserViewModel>(userServiceModel);

			return View(userViewModel);
		}

		[HttpGet]
		public async Task<IActionResult> Edit()
		{
			UserServiceModel userServiceModel = await this._userService.GetUserByClaimsAsync(this.HttpContext.User);

			if (userServiceModel == default(UserServiceModel))
				return RedirectToAction("Login");

			EditUserViewModel editUserViewModel = this._autoMapper.Map<EditUserViewModel>(userServiceModel);

			return View(editUserViewModel);
		}

		[HttpPost]
		public async Task<IActionResult> Edit(EditUserViewModel editUserViewModel)
		{
			if (!ModelState.IsValid)
				return View(editUserViewModel);

			if (!this._userService.IsSignedIn(HttpContext.User))
				return RedirectToAction("Login");

			UserServiceModel loggedInUser = await this._userService.GetUserByClaimsAsync(HttpContext.User);

			EditUserServiceModel editUserServiceModel = this._autoMapper.Map<EditUserServiceModel>(editUserViewModel);
			bool result = await this._userService.EditUserAsync(HttpContext.User, editUserServiceModel);

			if (result)
			{
				if (loggedInUser.Username != editUserViewModel.Username)
					await this._userService.LogoutAsync();

				return RedirectToAction("Profile", new { username = editUserViewModel.Username });
			}
			else
				return RedirectToAction("Profile", new { username = loggedInUser.Username });
		}

		[HttpPost]
		public async Task<IActionResult> Delete()
		{
			await this._userService.LogoutAsync();
			bool result = await this._userService.DeleteUserAsync(HttpContext.User);

			if (result)
				return RedirectToAction("Login");
			else
				return RedirectToAction("Index", "Home");
		}
	}
}
