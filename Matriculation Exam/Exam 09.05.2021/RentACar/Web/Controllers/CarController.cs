using RentACar.Services;
using Microsoft.AspNetCore.Mvc;
using RentACar.Web.Models.Car;
using AutoMapper;
using RentACar.Services.Models.Car;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using System.Linq;
using System.Collections.Generic;
using System;
using RentACar.Services.Models.CarRent;

namespace RentACar.Web.Controllers
{
	[Authorize]
	public class CarController : Controller
	{
		private readonly IMapper _autoMapper;
		private readonly CarService _carService;
		private readonly CarRentService _carRentService;

		public CarController(CarService carService, IMapper autoMapper, CarRentService carRentService)
		{
			this._carService = carService;
			this._autoMapper = autoMapper;
			this._carRentService = carRentService;
		}

		[HttpGet]
		public IActionResult Index(string onlyFree = null, DateTime startTime = default(DateTime), DateTime endTime = default(DateTime))
		{
			bool onlyFreeCars = true;
			List<CarServiceModel> serviceOut = null;
			if (onlyFree == "true")
				serviceOut = this._carService.GetFree();
			else if (startTime != default(DateTime) && endTime != default(DateTime))
				serviceOut = this._carService.GetBetweenDates(startTime, endTime);
			else if (startTime != default(DateTime))
				serviceOut = this._carService.GetFromStartDate(startTime);
			else if (endTime != default(DateTime))
				serviceOut = this._carService.GetFromEndDate(endTime);
			else
			{
				onlyFreeCars = false;
				serviceOut = this._carService.GetAll();
			}

			var model = serviceOut
				.Select(c => this._autoMapper.Map<CarViewModel>(c))
				.ToList();
			
			model.ForEach(c => c.IsFree = onlyFreeCars);

			return View(model);
		}

		[HttpGet]
		public async Task<IActionResult> Rent(string carId = null, double price = 0)
		{
			if (carId == null)
				return RedirectToAction("Index");

			CarServiceModel serviceOut = await this._carService.GetByIdAsync(Guid.Parse(carId));
			var model = this._autoMapper.Map<CarRentViewModel>(serviceOut);
			if (price > 0)
				model.TotalPrice = price;

			return View(model);
		}

		[HttpPost]
		public async Task<IActionResult> Rent(string carId = null, DateTime startTime = default(DateTime), DateTime endTime = default(DateTime), double totalPrice = 0)
		{
			if (carId == null || startTime == default(DateTime) || endTime == default(DateTime) || totalPrice <= 0)
				return RedirectToAction("Index");

			var serviceModel = new CreateCarRentServiceModel();
			serviceModel.StartDate = startTime;
			serviceModel.EndDate = endTime;
			serviceModel.CarId = Guid.Parse(carId);
			serviceModel.RentPrice = totalPrice;

			var result = await this._carRentService.Create(serviceModel, HttpContext.User);

			if (result)
				return RedirectToAction("Index");
			else
				return RedirectToAction("Rent", new { carId = carId, startTime = startTime.ToString("s"), endTime = endTime.ToString("s"), price = totalPrice });
		}

		[HttpPost]
		public async Task<IActionResult> GetPrice(string carId = null, DateTime startTime = default(DateTime), DateTime endTime = default(DateTime))
		{
			if (carId == null || startTime == default(DateTime) || endTime == default(DateTime))
				return RedirectToAction("Index");

			var total = await this._carService.CalculatePrice(Guid.Parse(carId), startTime, endTime);

			return RedirectToAction("Rent", new { carId = carId, startTime = startTime.ToString("s"), endTime = endTime.ToString("s"), price = total });
		}

	}
}
