using System.Collections.Generic;

namespace RentACar.Common
{
	public static class RentStatusConst
	{
		public static string Waiting = "Изчакваща";
		public static string Cancelled = "Отменена";
		public static string Active = "Активна";
		public static string Used = "Използвана";
		public static string Overdue = "Просрочена";

		public static List<string> GetAll()
		{
			return new List<string>()
			{
				Waiting, Cancelled, Active, Used, Overdue
			};
		}
	}
}
