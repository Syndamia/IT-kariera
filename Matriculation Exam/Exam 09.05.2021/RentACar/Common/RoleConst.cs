using System.Collections.Generic;

namespace RentACar.Common
{
	public static class RoleConst
	{
		public static string Client = "Client";
		public static string Administrator = "Administrator";

		public static List<string> GetAll()
		{
			return new List<string>()
			{
				Client, Administrator
			};
		}
	}
}
