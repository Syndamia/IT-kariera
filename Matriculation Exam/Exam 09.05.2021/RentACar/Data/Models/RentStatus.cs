using System;
using System.Collections.Generic;

namespace RentACar.Data.Models
{
    public class RentStatus
	{
		public Guid Id { get; set; }
		public string Name { get; set; }
		public HashSet<CarRent> CarRents { get; set; }
	}
}
