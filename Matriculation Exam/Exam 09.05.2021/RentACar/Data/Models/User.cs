using System;
using Microsoft.AspNetCore.Identity;
using System.ComponentModel.DataAnnotations.Schema;
using System.Collections.Generic;

namespace RentACar.Data.Models
{
	[Table("Users")]
	public class User : IdentityUser<Guid>
	{
		public string FirstName { get; set; }
		public string MiddleName { get; set; }
		public string LastName { get; set; }
		public string EGN { get; set; }
		public HashSet<CarRent> CarRents { get; set; }
	}
}
