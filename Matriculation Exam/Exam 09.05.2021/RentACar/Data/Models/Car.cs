using System;
using System.Collections.Generic;

namespace RentACar.Data.Models
{
	public class Car
	{
		public Guid Id { get; set; }
		public string Brand { get; set; }
		public string Model { get; set; }
		public int Year { get; set; }
		public int CountOfPassengerSeats { get; set; }
		public string Description { get; set; }
		public double RentPricePerDay { get; set; }
		public string Photo { get; set; }
		public HashSet<CarRent> CarRents { get; set; }
	}
}
