using System;

namespace RentACar.Data.Models
{
    public class CarRent
	{
		public Guid Id { get; set; }
		public DateTime StartDate { get; set; }
		public DateTime EndDate { get; set; }
		public Car RentedCar { get; set; }
		public User Author { get; set; }
		public double RentPrice { get; set; }
		public RentStatus Status { get; set; }
	}
}
