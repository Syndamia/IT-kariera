﻿using System;
using RentACar.Data.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace RentACar.Data
{
	public class RentACarContext : IdentityDbContext<User, IdentityRole<Guid>, Guid>
	{
		public RentACarContext(DbContextOptions<RentACarContext> options)
			: base(options) { }

		public DbSet<Car> Cars { get; set; }
		public DbSet<RentStatus> RentStatuses { get; set; }
		public DbSet<CarRent> CarRents { get; set; }

		protected override void OnModelCreating(ModelBuilder builder)
		{
			/* User config */

			builder.Entity<User>()
				.HasIndex(x => x.UserName)
				.IsUnique();

			builder.Entity<User>()
				.HasIndex(x => x.EGN)
				.IsUnique();

			/* CarRent config */

			builder.Entity<CarRent>()
				.HasOne(cr => cr.RentedCar)
				.WithMany(c => c.CarRents);

			builder.Entity<CarRent>()
				.HasOne(cr => cr.Author)
				.WithMany(a => a.CarRents);

			builder.Entity<CarRent>()
				.HasOne(cr => cr.Status)
				.WithMany(rs => rs.CarRents);

			base.OnModelCreating(builder);
		}
	}
}
