using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using System.IO;

namespace RentACar.Data
{
	public class RentACarContextFactory : IDesignTimeDbContextFactory<RentACarContext>
	{
		public RentACarContext CreateDbContext(string[] args)
		{
			var configuration = new ConfigurationBuilder()
				.SetBasePath(Directory.GetCurrentDirectory() + "/../Web/")
				.AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
				.AddJsonFile("appsettings.Development.json", optional: true)
				.Build();

			var optionsBuilder = new DbContextOptionsBuilder<RentACarContext>()
				.UseNpgsql(configuration.GetConnectionString("LocalDBConnection"));

			return new RentACarContext(optionsBuilder.Options);
		}
	}
}
