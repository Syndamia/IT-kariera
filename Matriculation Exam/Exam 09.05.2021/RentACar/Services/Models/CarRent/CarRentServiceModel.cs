using System;

namespace RentACar.Services.Models.CarRent
{
    public class CarRentServiceModel
	{
		public Guid Id { get; set; }
		public DateTime StartDate { get; set; }
		public DateTime EndDate { get; set; }
		public Guid CarId { get; set; }
		public Guid AuthorId { get; set; }
		public double RentPrice { get; set; }
	}
}
