using System;

namespace RentACar.Services.Models.Car
{
	public class CarServiceModel
	{
		public Guid Id { get; set; }
		public string Brand { get; set; }
		public string Model { get; set; }
		public int Year { get; set; }
		public int CountOfPassengerSeats { get; set; }
		public string Description { get; set; }
		public double RentPricePerDay { get; set; }
		public string Photo { get; set; }
	}
}
