namespace RentACar.Services.Models.Car
{
	public class CreateCarServiceModel
	{
		public string Brand { get; set; }
		public string Model { get; set; }
		public int Year { get; set; }
		public int CountOfPassengerSeats { get; set; }
		public string Description { get; set; }
		public double RentPricePerDay { get; set; }
		public string Photo { get; set; }
	}
}
