using AutoMapper;
using RentACar.Data.Models;
using RentACar.Services.Models.Car;

namespace RentACar.Services.Configurations
{
    public class CarMappings : Profile
	{
		public CarMappings()
		{
			CreateMap<Car, CarServiceModel>();
		}
	}
}
