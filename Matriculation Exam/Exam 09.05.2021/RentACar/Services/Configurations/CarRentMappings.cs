using AutoMapper;
using RentACar.Data.Models;
using RentACar.Services.Models.CarRent;

namespace RentACar.Services.Configurations
{
    public class CarRentMappings : Profile
	{
		public CarRentMappings()
		{
			CreateMap<CreateCarRentServiceModel, CarRent>();
		}
	}
}
