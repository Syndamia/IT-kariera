using AutoMapper;
using RentACar.Data.Models;
using RentACar.Services.Models.User;

namespace RentACar.Services.Configurations
{
	public class UserMappings : Profile
	{
		public UserMappings()
		{
			CreateMap<RegisterUserServiceModel, User>();
			CreateMap<User, UserServiceModel>();
		}
	}
}
