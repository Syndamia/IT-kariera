using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using RentACar.Common;
using RentACar.Data;
using RentACar.Data.Models;
using RentACar.Services.Models.Car;

namespace RentACar.Services
{
    public class CarService: BaseService
	{
		private readonly CloudinaryService _cloudService;

		public CarService(IMapper autoMapper, RentACarContext rentACarContext, CloudinaryService cloudService)
			:base (autoMapper, rentACarContext)
		{
			this._cloudService = cloudService;
		}

		public async Task<bool> Create(CreateCarServiceModel createCarServiceModel)
		{
			Car newCar = this._autoMapper.Map<Car>(createCarServiceModel);

			await this._context.Cars
				.AddAsync(newCar);

			return await this._context.SaveChangesAsync() >= 1;
		}

		public async Task<CarServiceModel> GetByIdAsync(Guid id)
		{
			Car car = await this._context.Cars
				.FindAsync(id);

			return this._autoMapper.Map<CarServiceModel>(car);
		}

		public List<CarServiceModel> GetAll()
		{
			return this._context.Cars
				.Select(c => this._autoMapper.Map<CarServiceModel>(c))
				.ToList();
		}

		public List<CarServiceModel> GetFree()
		{
			return this._context.Cars
				.Include(c => c.CarRents)
				.Where(c => !c.CarRents.Any(cr => cr.Status.Name == RentStatusConst.Active ||
							cr.Status.Name == RentStatusConst.Overdue))
				.Select(c => this._autoMapper.Map<CarServiceModel>(c))
				.ToList();
		}

		// The following three methods are utterly stupid

		public List<CarServiceModel> GetFromStartDate(DateTime startDate)
		{
			return this._context.Cars
				.Include(c => c.CarRents)
				.Where(c => !c.CarRents.Any(cr => cr.StartDate >= startDate && (cr.Status.Name == RentStatusConst.Active ||
							cr.Status.Name == RentStatusConst.Overdue)))
				.Select(c => this._autoMapper.Map<CarServiceModel>(c))
				.ToList();
		}

		public List<CarServiceModel> GetFromEndDate(DateTime endDate)
		{
			return this._context.Cars
				.Include(c => c.CarRents)
				.Where(c => !c.CarRents.Any(cr => cr.EndDate <= endDate && (cr.Status.Name == RentStatusConst.Active ||
							cr.Status.Name == RentStatusConst.Overdue)))
				.Select(c => this._autoMapper.Map<CarServiceModel>(c))
				.ToList();
		}

		public List<CarServiceModel> GetBetweenDates(DateTime startDate, DateTime endDate)
		{
			return this._context.Cars
				.Include(c => c.CarRents)
				.Where(c => !c.CarRents.Any(cr => cr.StartDate >= startDate && cr.EndDate <= endDate && (cr.Status.Name == RentStatusConst.Active ||
							cr.Status.Name == RentStatusConst.Overdue)))
				.Select(c => this._autoMapper.Map<CarServiceModel>(c))
				.ToList();
		}

		public async Task<bool> EditAsync(EditCarServiceModel editCarServiceModel)
		{
			Car car = await this._context.Cars
				.FindAsync(editCarServiceModel.Id);

			car.Brand = editCarServiceModel.Brand;
			car.Model = editCarServiceModel.Model;
			car.Year = editCarServiceModel.Year;
			car.CountOfPassengerSeats = editCarServiceModel.CountOfPassengerSeats;
			car.Description = editCarServiceModel.Description;
			car.RentPricePerDay = editCarServiceModel.RentPricePerDay;
			car.Photo = (await this._cloudService.UploadFilesToCloud(new List<IFormFile>() { editCarServiceModel.PhotoFile }))[0];

			return await this._context.SaveChangesAsync() >= 1;
		}

		public async Task<bool> DeleteAsync(Guid id)
		{
			Car car = await this._context.Cars
				.FindAsync(id);

			this._context.Remove(car);

			return await this._context.SaveChangesAsync() >= 1;
		}

		public async Task<bool> IsFree(Guid id)
		{
			Car car = await this._context.Cars
				.Include(c => c.CarRents)
				.FirstOrDefaultAsync(c => c.Id == id);

			return !car.CarRents.Any(cr => cr.Status.Name == RentStatusConst.Active ||
							cr.Status.Name == RentStatusConst.Overdue);
		}

		public async Task<double> CalculatePrice(Guid id, DateTime startDate, DateTime endDate)
		{
			double period = endDate.Subtract(startDate).Days;
			Car car = await this._context.Cars
				.FindAsync(id);
			return period * car.RentPricePerDay;
		}
	}
}
