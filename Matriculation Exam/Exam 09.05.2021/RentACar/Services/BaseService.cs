using AutoMapper;
using RentACar.Data;

namespace RentACar.Services
{
    public abstract class BaseService
	{
		protected readonly IMapper _autoMapper;
		protected readonly RentACarContext _context;

		public BaseService(IMapper autoMapper, RentACarContext rentACarContext)
		{
			this._autoMapper = autoMapper;
			this._context = rentACarContext;
		}
	}
}
