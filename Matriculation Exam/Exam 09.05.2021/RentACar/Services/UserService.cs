﻿using System;
using System.Security.Claims;
using System.Threading.Tasks;
using AutoMapper;
using RentACar.Data;
using RentACar.Data.Models;
using RentACar.Services.Models.User;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using RentACar.Common;

namespace RentACar.Services
{
	public class UserService
	{
		private readonly IMapper _autoMapper;
		private readonly RentACarContext _context;
		private readonly SignInManager<User> _signInManager;
		private readonly UserManager<User> _userManager;
		private readonly RoleManager<IdentityRole<Guid>> _roleManager;

		public UserService(IMapper autoMapper, RentACarContext rentACarContext, SignInManager<User> signInManager, UserManager<User> userManager, RoleManager<IdentityRole<Guid>> roleManager)
		{
			this._autoMapper = autoMapper;
			this._context = rentACarContext;
			this._signInManager = signInManager;
			this._userManager = userManager;
			this._roleManager = roleManager;
		}

		public async Task<bool> RegisterUserAsync(RegisterUserServiceModel registerUserServiceModel)
		{
			User user = this._autoMapper.Map<User>(registerUserServiceModel);

			user.PasswordHash = this._userManager.PasswordHasher.HashPassword(user, registerUserServiceModel.Password);
			IdentityResult userCreateResult = await this._userManager.CreateAsync(user);
			IdentityResult addRoleResult = await this._userManager.AddToRoleAsync(user, RoleConst.Client);

			return userCreateResult.Succeeded && addRoleResult.Succeeded;
		}

		public async Task<bool> LoginUserAsync(LoginUserServiceModel loginUserServiceModel)
		{
			SignInResult result = await this._signInManager.PasswordSignInAsync(loginUserServiceModel.Username, loginUserServiceModel.Password, false, false);

			return result.Succeeded;
		}

		public async Task LogoutAsync()
		{
			await this._signInManager.SignOutAsync();
		}

		public async Task<UserServiceModel> GetUserByUsernameAsync(string username)
		{
			User user = await this._userManager.Users
				.FirstOrDefaultAsync(x => x.UserName == username);

			return this._autoMapper.Map<UserServiceModel>(user);
		}

		public async Task<UserServiceModel> GetUserByClaimsAsync(ClaimsPrincipal claimsPrincipal)
		{
			User user = await this._userManager.GetUserAsync(claimsPrincipal);

			return this._autoMapper.Map<UserServiceModel>(user);
		}

		public async Task<bool> EditUserAsync(ClaimsPrincipal claimsPrincipal, EditUserServiceModel editUserServiceModel)
		{
			User user = await this._userManager.GetUserAsync(claimsPrincipal);

			user.FirstName = editUserServiceModel.FirstName;
			user.MiddleName = editUserServiceModel.MiddleName;
			user.LastName = editUserServiceModel.LastName;
			user.EGN = editUserServiceModel.EGN;
			user.PhoneNumber = editUserServiceModel.PhoneNumber;
			user.Email = editUserServiceModel.Email;
			user.UserName = editUserServiceModel.Username;

			IdentityResult result = await this._userManager.UpdateAsync(user);
			return result.Succeeded;
		}

		public async Task<bool> DeleteUserAsync(ClaimsPrincipal claimsPrincipal)
		{
			User user = await this._userManager.GetUserAsync(claimsPrincipal);

			IdentityResult result = await this._userManager.DeleteAsync(user);
			return result.Succeeded;
		}

		public bool IsSignedIn(ClaimsPrincipal claimsPrincipal)
		{
			return this._signInManager.IsSignedIn(claimsPrincipal);
		}
	}
}
