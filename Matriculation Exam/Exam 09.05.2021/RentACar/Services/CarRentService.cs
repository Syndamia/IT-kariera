using System;
using System.Security.Claims;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using RentACar.Common;
using RentACar.Data;
using RentACar.Data.Models;
using RentACar.Services.Models.CarRent;

namespace RentACar.Services
{
    public class CarRentService
	{
		private readonly IMapper _autoMapper;
		private readonly RentACarContext _context;
		private readonly UserManager<User> _userManager;

		public CarRentService(IMapper autoMapper, RentACarContext rentACarContext, UserManager<User> userManager)
		{
			this._autoMapper = autoMapper;
			this._context = rentACarContext;
			this._userManager = userManager;
		}

		public async Task<bool> Create(CreateCarRentServiceModel createCarRentServiceModel, ClaimsPrincipal claimsPrincipal)
		{
			CarRent newCarRent = this._autoMapper.Map<CarRent>(createCarRentServiceModel);
			newCarRent.Author = await this._userManager.GetUserAsync(claimsPrincipal);
			newCarRent.RentedCar = await this._context.Cars
				.FirstAsync(c => c.Id == createCarRentServiceModel.CarId);
			newCarRent.Status = await this._context.RentStatuses
				.FirstAsync(rs => rs.Name == RentStatusConst.Active);

			await this._context.CarRents
				.AddAsync(newCarRent);

			return await this._context.SaveChangesAsync() >= 1;
		}

		public async Task<CarRentServiceModel> GetByIdAsync(Guid id)
		{
			CarRent carRent = await this._context.CarRents
				.FindAsync(id);

			return this._autoMapper.Map<CarRentServiceModel>(carRent);
		}

		public async Task<bool> EditAsync(EditCarRentServiceModel editCarRentServiceModel)
		{
			CarRent carRent = await this._context.CarRents
				.FindAsync(editCarRentServiceModel.Id);

			carRent.StartDate = editCarRentServiceModel.StartDate;
			carRent.EndDate = editCarRentServiceModel.EndDate;
			carRent.RentedCar = await this._context.Cars.FindAsync(editCarRentServiceModel.CarId);
			carRent.Author = await this._context.Users.FindAsync(editCarRentServiceModel.AuthorId);
			carRent.RentPrice = editCarRentServiceModel.RentPrice;

			return await this._context.SaveChangesAsync() >= 1;
		}

		public async Task<bool> DeleteAsync(Guid id)
		{
			CarRent carRent = await this._context.CarRents
				.FindAsync(id);

			this._context.Remove(carRent);

			return await this._context.SaveChangesAsync() >= 1;
		}
	}
}
